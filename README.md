# Contributing guidelines

This repository contains guidelines for contributing to the [Clean][] projects
that I maintain. Thank you for contributing!

[[_TOC_]]

## General workflow

We use [semantic versioning](https://semver.org) as described in
[the Nitrile documentation](https://clean-and-itasks.gitlab.io/nitrile/packaging-and-publishing/checklist/).

We use branches in the following way:

- The `main` branch is used for the latest version.
- In preparation of a new major or minor version, a branch `pre-x.y` is used
  (e.g. `pre-2.0`).
- Bug fixes should be done on `main` or a `pre-x.y` branch.
- If bug fixes need to be backported to an older version, a branch
  `release-x.y` is used (e.g. `release-1.3`).

Issue tracking and merge request guidelines:

- Bugs and feature proposals are tracked in the issue tracker.
- Once code is ready to be merged into `main` this is done in a merge request.
- Merge requests are assigned to the person who currently has to work on it.
  This can be a reviewer when the code is ready for review, or somebody else,
  if more work is required before the MR can be merged. Merge requests that are
  not ready for review should also be marked as Drafts.
- Comment threads are resolved by the person who started them, not the person
  who answers them. This makes sure that everyone has an overview of what
  remains to be done.

## Style guide

In general, follow the style of the surrounding code. Rationale and some rules
of thumb can be found [here](STYLE_GUIDE.md).

## Testing

- Existing tests should be adapted if required so that the CI pipeline passes.
- For fixed bugs there should be a new test, which failed before the MR and
  passes after the MR.

## Documentation

- All changed/added exported functions and types should be correctly documented
  in the definition module according to the
  [Platform documentation standards](https://gitlab.com/clean-and-itasks/clean-platform/-/blob/master/doc/DOCUMENTATION.md).
- In implementation modules unusual choices (e.g. fancy optimisations) or parts
  which are likely to get broken in the future (e.g. arguments which have to
  remain lazy) should be documented.
- *What* code does should become clear from the code itself. Good structure and
  naming is important to achieve this. Comments should explain *why* certain
  non-straightforward choices have been made.

## Changelog

Each repository contains a file `CHANGELOG.md`.

At the top of the file there is a heading `Unreleased`. If you make a change
and want to add a changelog entry, add it here. You may add the `Unreleased`
heading if there is none.

### Which changes require an entry?

In general, use common sense, or ask if unsure. Here are some guidelines:

- New features should always get an entry.
- Backwards incompatible changes should always get an entry.
- Bug fixes should always get an entry, unless they fix a bug that has not been
  released yet.
- Enhancements of existing features should get an entry if they are observable
  for end users.
- Changes in documentation and refactoring do not need to get an entry.
- Changes by outside contributors may always get an entry.

### Entries

Changes can have different types. The entries should be grouped by type, in
this order:

- `Feature` for new user-facing functionality.
- `Enhancement` for enhanced functionality without API changes.
- `Change` for user-facing changes in functionality.
- `Removed` for removed functionality.
- `Fix` for fixed bugs.
- `Robustness` for changes to catch edge cases.
- `Chore` for updates to dependencies and the like.

Each entry should give a short explanation and a link to the corresponding
merge request or commit. Entries may include the name / GitLab handle / email
address of the contributor.

The entry explanation starts with a lower-case letter and ends with a full
stop. For example:

```md
- Feature: add ability to foo bars.
```

### Headings

- Headings are sorted in descending order.
- Major versions (x.0.0) and minor versions in the major version 0 (0.x.0)
  should be on heading level 2 (`##`).
- Minor versions in non-zero major versions (x.y.0) should be on heading level
  3 (`###`).
- Patch versions (x.y.z) should be on heading level 4 (`####`).
- There may be a heading `## Unreleased` at the top of the file for unreleased
  changes. The level should be the one that the version gets when it is
  released. That is, if there are unreleased changes that require a major
  version update, the level should be `##`. If the unreleased changes only
  require a patch update, the level should be `####`.

[Clean]: https://clean-lang.org
