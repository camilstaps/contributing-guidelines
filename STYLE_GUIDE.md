# Style guide

In general, please follow the style of surrounding code. This file gives some
explanations on some of the choices. It is not complete, but based on
experience of the things that most often go wrong (and are therefore more
likely to be different from what you would expect).

[[_TOC_]]

## A. Clean

### A1. General layout

#### A1.1. White space, line limits, etc.

1. Use tabs for indent and spaces for alignment.
1. There is no strict line limit, but you could consider splitting a line when
   it goes over ±120 characters. Follow examples elsewhere in the code.
1. Don't use spaces in patterns, but do use spaces in expressions. This makes it
   easier to read the code, as you can immediately see whether something is a
   pattern or an expression:

   ```clean
   g records = [{x=y, y=x} \\ {x,y} <- records]

   h x
       # (x,y) = h` x
       = (y, x)
   ```
1. Contrary to the previous rule, list expressions do not need to be written
   with spaces if they are small enough. But when spaces are used, use only a
   space after `,` but spaces on either side of `:`.

   ```clean
   f [a,b:rest] = [b, a : rest] // or:
   f [a,b:rest] = [b,a:rest]
   ```
1. Use spaces around the `\\` and `<-` of list/array comprehensions.
1. Spaces are not needed around arithmetic operators when their operands are
   simple enough.

#### A1.2. Guards and lets

1. Indent `#` and `|` with a tab.
1. Combine semantically related lets by leaving out the `#`.

   ```clean
   f x
       # a = computeA x
         b = computeB x
       = (a, b)
   ```
1. In particular, use this for `fromJust`/`fromOk` *before* a guard checking for
   `isJust`/`isOk`. Use `mb` followed by the expression name starting with a
   capital letter, i.e.:

   ```clean
   g x
       # mbA = tryComputeA x
         a = fromJust mbA
       | isNone mbA = Error "failed to compute a"
       # mbB = tryComputeB x
         b = fromOk mbB
       | isError mbB = Error "failed to compute b"
       | otherwise = Ok (a, b)
   ```
1. The general rule is: the `=` symbols of guards on the same level
   syntactically should be aligned. (This also goes for the `->` symbols of
   `case`s on the same level.) Examples:

   ```clean
   // OK:                            // OK:
   f x                               case mbX of
       | x > 0 = f (x-1)                 ?Just x -> Ok x
       | otherwise = ()                  ?None -> Error "no value"

   // OK:                            // OK:
   f x                               case mbX of
       | x > 0                           ?Just x ->
           = f (x-1)                         Ok x
           = ()                          ?None ->
                                             Error "no value"

   // Not OK:                        // Not OK:
   f x                               case mbX of
       | x > 0                           ?None -> Error "no value"
           = f (x-1)                     ?Just x ->
       = ()                                  Ok x

   // Not OK:
   f x
       | x > 0 = f (x-1)
       = ()
   ```

   This also applies when lets intervene:

   ```clean
   // Not OK:
   f mbX
       | isNone mbX = Error "none"   // = should be on new line to match the = symbols below
       # (x,y) = g (fromJust mbX)
       | x > 0
           = Ok True
           = Ok (y > x)
   ```
1. An exception to the previous rule is the last `=`, when preceded by a let:

   ```clean
   // OK:
   f x
       | x > 0
           = f (x-1)
       # a = computeA x
       = a + a
   ```
1. With `case`s it is generally preferable to have the `->` on the line of the
   pattern (if using multiple lines). But when lets or guards are used, the
   `->` must all be on the line of the expression:

   ```clean
   // Normally preferable:
   case tuple of
       (x,y) ->
           y + x

   // With a let:
   case tuple of
       (0,y)
           -> y           // -> on this line!
       (x,y)
           # sum = x + y
           -> f x sum
   ```

#### A1.3. `->`, `=`, and `.`

1. Use `->` for `case`s, also when they have lets (`#`) and/or guards.
1. Use `->` surrounded by spaces for lambda expressions: `\x -> ...`.

#### A1.4. `$`

1. Avoid the use of `$` within parentheses:

   ```clean
   f = map (\(n,xs) -> seqSt g xs $ init n) xs // wrong
   f = map (\(n,xs) -> seqSt g xs (init n)) xs // preferred
   ```

#### A1.5. Monadic operators

1. Use `<$&>` and `=<<` (instead of `<$>` and `>>=`, respectively) when this
   makes the code more readable. `<$&>` is often preferable when the operation
   is conceptually more "sequential" (because it places the function *after*
   the input), while `=<<` is preferable when the operation is more
   "pure"/"arithmetical" (because it places the function *before* the
   parameter).

### A2. Implementation choices

1. Use `map` and not `<$>` for lists. In general, use functions that make it
   clear what type is being used (similarly, use `+1` instead of `inc` for
   integers).
1. Combinations of functions on lists can often be made more readable with a
   list comprehension.
